# Screecher 

Your first ever cliffracer-themed pomodoro clock. Does what every pomodoro clock should do: measure time and screech at you.  

## Details

Simple side project created with java + javafx and finished in Eclipse IDE  

![Main window](SC/main.png)

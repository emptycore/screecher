package sample;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;


import java.util.Timer;
import java.util.TimerTask;
import java.io.File;


public class Controller {
    @FXML
    TextField interval;
    @FXML
    TextField waiting;
    @FXML
    Label status;
    @FXML
    Button scre;

    protected void initialize(){ //initialize and protect textfields from the garbage
        waiting.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    waiting.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        interval.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    interval.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }


    class screechWork extends TimerTask { //End of the relaxing interval
        int count=0;
        public void run() {
            count++;
            String musicFile = "resources/moan.wav";
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.setCycleCount(count);
            mediaPlayer.play();
            Platform.runLater(() -> {
            status.setText("Work");
            scre.setText("Wait for pause");
            scre.setDisable(true);
            });
            if (count==3){
                this.cancel();
                Platform.runLater(() -> {
                    status.setText("Final stretch");
                });
            }


        }
    }
    class screechRelax extends TimerTask { //End of the working interval
        int count=0;
        public void run() {
            count++;
            String musicFile = "resources/roar.wav";
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.setCycleCount(count);
            mediaPlayer.play();
            Platform.runLater(() -> {
                status.setText("Number of passed intervals: "+ count +". Relax. You can restart the countdown right now, if you want it");
                scre.setDisable(false);
                scre.setText("Restart");
            });
            if (count==4) {
                this.cancel();
                Platform.runLater(() -> {
                status.setText("You've done here. # of passed intervals: "+ count);
                });
            }
        }
    }
    public int checkBounds(TextField textField){ //convert user input into milliseconds
        final int msToMinute = 60000;
        int min = 1;
        int max = 2880;
            int result = Integer.parseInt(textField.getText());
            if (result < min) {
                result = min;
                textField.setText(String.valueOf(min));
            }
            if (result > max) {
                result = max;
                textField.setText(String.valueOf(max));
            }
            return result*msToMinute;

    }
    public void setTime(){ //PUSH. THE. BUTTON
        int pause = checkBounds(waiting);
        int work = checkBounds(interval);
        Timer intervalT = new Timer();
        Timer pauseT = new Timer();
        TimerTask taskI = new screechRelax();
        TimerTask taskP=new screechWork();
        intervalT.scheduleAtFixedRate(taskI, work, work+pause);
        pauseT.scheduleAtFixedRate(taskP, work+pause,work+pause);
        status.setText("Go!");
        status.setStyle("-fx-font-weight:bold;-fx-text-alignment:center;");
        scre.setText("Wait for pause");
        scre.setDisable(true);
    }
}

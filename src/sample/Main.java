package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image ;

import java.io.File;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        String appIcon = "resources/scree.png";
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Screecher");
        primaryStage.setScene(new Scene(root, 168, 260));
        primaryStage.show();
        primaryStage.setResizable(false);
        primaryStage.setOnCloseRequest(e -> handleExit());
        primaryStage.getIcons().add(new Image(new File(appIcon).toURI().toString()));
    }

    public static void main(String[] args) {
        launch(args);
    } //Thank god for clean exits
    private void handleExit() {
        Platform.exit();
        System.exit(0);
    }

}